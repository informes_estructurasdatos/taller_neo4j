from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from typing import Optional
from py2neo import Graph, ClientError, DatabaseError

app = FastAPI()

# Configurar la conexión a Neo4j
uri = "bolt://137.184.41.191:7687"
user = "neo4j"
password = "Contraseña"
try:
    graph = Graph(uri, auth=(user, password))
except ClientError as e:
    raise HTTPException(status_code=500, detail="Error al conectar con la base de datos") from e

class Usuario(BaseModel):
    nombre: str
    signo: Optional[str] = None
    equipo: Optional[str] = None
    delito: Optional[str] = None
    bebida_favorita: Optional[str] = None

# Función para crear un nuevo usuario
def create_usuario(usuario: Usuario):
    query = """
    CREATE (u:Usuario {nombre: $nombre, signo: $signo, equipo: $equipo, delito: $delito, bebida_favorita: $bebida_favorita})
    """
    try:
        graph.run(query, nombre=usuario.nombre, signo=usuario.signo, equipo=usuario.equipo, delito=usuario.delito,
                  bebida_favorita=usuario.bebida_favorita)
    except DatabaseError as e:
        raise HTTPException(status_code=500, detail="Error al crear el usuario en la base de datos") from e

def connect_usuarios_mismo_signo(signo: str):
    query = f"""
    MATCH (u1:Usuario {{signo: '{signo}'}}), (u2:Usuario {{signo: '{signo}'}})
    WHERE id(u1) <> id(u2)
    MERGE (u1)-[:{signo}]->(u2)
    """
    try:
        graph.run(query)
    except DatabaseError as e:
        raise HTTPException(status_code=500, detail="Error al conectar usuarios con el mismo signo") from e

def connect_usuarios_mismo_equipo(equipo: str):
    query = f"""
    MATCH (u1:Usuario {{equipo: '{equipo}'}}), (u2:Usuario {{equipo: '{equipo}'}})
    WHERE id(u1) <> id(u2)
    MERGE (u1)-[:{equipo}]->(u2)
    """
    try:
        graph.run(query)
    except DatabaseError as e:
        raise HTTPException(status_code=500, detail="Error al conectar usuarios con el mismo equipo") from e

def connect_usuarios_mismo_delito(delito: str):
    query = f"""
    MATCH (u1:Usuario {{delito: '{delito}'}}), (u2:Usuario {{delito: '{delito}'}})
    WHERE id(u1) <> id(u2)
    MERGE (u1)-[:{delito}]->(u2)
    """
    try:
        graph.run(query)
    except DatabaseError as e:
        raise HTTPException(status_code=500, detail="Error al conectar usuarios con el mismo delito") from e

def connect_usuarios_misma_bebida(bebida: str):
    query = f"""
    MATCH (u1:Usuario {{bebida_favorita: '{bebida}'}}), (u2:Usuario {{bebida_favorita: '{bebida}'}})
    WHERE id(u1) <> id(u2)
    MERGE (u1)-[:{bebida}]->(u2)
    """
    try:
        graph.run(query)
    except DatabaseError as e:
        raise HTTPException(status_code=500, detail="Error al conectar usuarios con la misma bebida favorita") from e

# Endpoint para crear un nuevo usuario
@app.post("/crear_usuario/")
async def crear_usuario(usuario: Usuario):
    try:
        create_usuario(usuario)
        connect_usuarios_mismo_signo(usuario.signo)
        if usuario.equipo:
            connect_usuarios_mismo_equipo(usuario.equipo)
        if usuario.delito:
            connect_usuarios_mismo_delito(usuario.delito)
        if usuario.bebida_favorita:
            connect_usuarios_misma_bebida(usuario.bebida_favorita)
        return {'mensaje': 'Usuario creado exitosamente'}
    except HTTPException as e:
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail="Error inesperado al crear el usuario") from e

# Endpoint para obtener todos los usuarios
@app.get("/usuarios/")
async def get_all_users():
    query = """
    MATCH (u:Usuario)
    RETURN u.nombre
    """
    try:
        results = graph.run(query)
        nombres = [result[0] for result in results]
        return {'nombres': nombres}
    except DatabaseError as e:
        raise HTTPException(status_code=500, detail="Error al obtener los usuarios de la base de datos") from e

# Endpoint para eliminar un usuario por nombre
@app.delete("/eliminar_usuario/{nombre}")
async def eliminar_usuario(nombre: str):
    query = """
    MATCH (u:Usuario {nombre: $nombre})
    DETACH DELETE u
    """
    try:
        result = graph.run(query, nombre=nombre)
        if result.stats()['nodes_deleted'] == 0:
            raise HTTPException(status_code=404, detail="Usuario no encontrado")
        return {'mensaje': 'Usuario eliminado exitosamente'}
    except DatabaseError as e:
        raise HTTPException(status_code=500, detail="Error al eliminar el usuario de la base de datos") from e

# Endpoint para actualizar los campos de un usuario
@app.put("/actualizar_usuario/{nombre}")
async def actualizar_usuario(nombre: str, usuario: Usuario):
    query = """
    MATCH (u:Usuario {nombre: $nombre})
    RETURN u
    """
    try:
        result = graph.run(query, nombre=nombre).data()
        if not result:
            raise HTTPException(status_code=404, detail="Usuario no encontrado")
        current_data = result[0]['u']
        new_data = {
            'signo': usuario.signo if usuario.signo is not None else current_data.get('signo'),
            'equipo': usuario.equipo if usuario.equipo is not None else current_data.get('equipo'),
            'delito': usuario.delito if usuario.delito is not None else current_data.get('delito'),
            'bebida_favorita': usuario.bebida_favorita if usuario.bebida_favorita is not None else current_data.get(
                'bebida_favorita')
        }
        query = """
        MATCH (u:Usuario {nombre: $nombre})
        SET u.signo = $signo,
            u.equipo = $equipo,
            u.delito = $delito,
            u.bebida_favorita = $bebida_favorita
        """
        graph.run(query, nombre=nombre, **new_data)
        if usuario.equipo:
            connect_usuarios_mismo_equipo(usuario.equipo)
        if usuario.delito:
            connect_usuarios_mismo_delito(usuario.delito)
        if usuario.bebida_favorita:
            connect_usuarios_misma_bebida(usuario.bebida_favorita)
        return {'mensaje': 'Usuario actualizado exitosamente'}
    except HTTPException as e:
        raise e
    except DatabaseError as e:
        raise HTTPException(status_code=500, detail="Error al actualizar el usuario en la base de datos") from e
    except Exception as e:
        raise HTTPException(status_code=500, detail="Error inesperado al actualizar el usuario") from e